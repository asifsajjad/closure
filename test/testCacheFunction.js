const cacheFunction= require('../cacheFunction');

function testCacheFunction(userFn){
    return cacheFunction(userFn);
}

const myFun=(x,y)=>{
    return x*y;
}

const square=testCacheFunction(myFun);
console.log(square(2,3));
console.log(square(4,3));
console.log(square(2,3));
