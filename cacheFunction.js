function cacheFunction(cb) {

    const cache = new Map();


    return (...userIn) => {
        let result = [];
        key = JSON.stringify(userIn);

        if (cache.has(key)) {
            // console.log('Got from cache!');
            result = cache.get(key);
        }
        else {
            result = cb(...userIn);
            cache.set(key,result)
        }
        return result;

    };
}

module.exports = cacheFunction;