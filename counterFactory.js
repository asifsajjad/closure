function counterFactory() {

    let storedCount = 0;

    return {
        increment: () => {
            storedCount++;
            return storedCount;
        },
        decrement: () => {
            storedCount--;
            return storedCount;
        }
    };
}



module.exports = counterFactory;