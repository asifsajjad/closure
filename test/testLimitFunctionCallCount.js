const limitFunctionCallCount = require('../limitFunctionCallCount');

function testLimitFunctionCallCount(userCb, userN) {
    return limitFunctionCallCount(userCb, userN);
}

const myFun = () => {
    // console.log('the output.');
    const a =7
    return a;
}

const returnedFn = testLimitFunctionCallCount(myFun, 4);

console.log(returnedFn());
console.log(returnedFn());
console.log(returnedFn());
console.log(returnedFn());
console.log(returnedFn());
console.log(returnedFn());
