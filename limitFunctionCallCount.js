const counterFactory = require('./counterFactory');

const counter = counterFactory();

function limitFunctionCallCount(cb, n) {

    return (...args) => {

        if (counter.increment() <= n) {
            return cb(...args);
        }
        else {
            return null;
        }
    };

}


module.exports = limitFunctionCallCount;