const counterFactory = require('../counterFactory');

function testCounterFactory() {
    return counterFactory();
}

const counter = testCounterFactory();

console.log(counter.increment());
console.log(counter.decrement());
